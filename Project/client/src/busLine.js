function fetchBusStop(){
    fetch("http://localhost:8080/trafiklab/getBusLineWithMostStop")
        .then(response => {
                if(!response.ok){
                    response.json().then(error => {
                        console.log(error.message)
                    })
                    throw new Error("Request failed!")
                }
                return response.json()
            }
        ).then(data => {
            removeLoadingElement()

            if(!data || !data.length)
                addMessage("No data found!")
            else{
                data.forEach(busLine => {
                    const id = "collapsible"+busLine.busLine
                    addLineToDocument(id, busLine)
                    addEventListenerToLine(id)
                })
            }
    }).catch(error => {
        removeLoadingElement()
        logErrorMessage(error.message)
    })
}

function removeLoadingElement(){
    if(document.getElementById("loadingElement")!=null)
        document.getElementById("loadingElement").remove()
}

function logErrorMessage(message){
    addMessage("Could not fetch data!")
    console.log(message)
}

function addMessage(message){
    document.querySelector("#app").insertAdjacentHTML("afterend", message)
}

function addLineToDocument(id, busLine){
    var innerhtml=``
    busLine.stop.forEach(line => {
        innerhtml = innerhtml + `<p>Stop: ${line}</p>`
    })

    const html = `<button id=${id} class="collapsible">Bus line ${busLine.busLine}</button> <div class = "content">` +
        innerhtml + `</div>`
    document.querySelector("#app").insertAdjacentHTML("afterend", html)
}

function addEventListenerToLine(id){
    var coll = document.getElementById(id)
    coll.addEventListener("click", function() {
        this.classList.toggle("active")
        var content = this.nextElementSibling
        if (content.style.maxHeight){
            content.style.maxHeight = null
        } else {
            content.style.maxHeight = content.scrollHeight + "px"
        }
    });
}

fetchBusStop()