package server.core;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import server.core.model.BusLine;
import server.integration.trafiklab.TrafiklabClient;
import server.integration.trafiklab.model.Jour;
import server.integration.trafiklab.model.Line;
import server.integration.trafiklab.model.Stop;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Service
public class TraficlabService {
    private TrafiklabClient trafiklabClient;
    private static BeanUtil beanUtil;

    private final Logger log = LoggerFactory.getLogger(TraficlabService.class);

    @Autowired
    public TraficlabService(TrafiklabClient trafiklabClient, BeanUtil beanUtil) {
        this.trafiklabClient = trafiklabClient;
        this.beanUtil = beanUtil;
    }

    public List<BusLine> getListOfBusLine(){
        Line line = trafiklabClient.getLine();
        Stop stop = trafiklabClient.getStop();
        Jour jour = trafiklabClient.getJour();
        return createBusList(line, stop, jour, 10);
    }

    public List<BusLine> createBusList(Line line, Stop stop, Jour jour, int sizeOfList){
        List<BusLine> listOfBusLine = new ArrayList();

        line.getResponseData().getResult().forEach(e -> {
            log.info("Loading bus line "+e.getLineNumber());
            BusLine ib = new BusLine(e.getLineNumber());
            jour.getResponseData().getResult().forEach(e2 -> {
                if(e.getLineNumber().equals(e2.getLineNumber())) {
                    stop.getResponseData().getResult().forEach(e3 -> {
                        if(e2.getJourneyPatternPointNumber().equals(e3.getStopPointNumber())
                        && e2.getDirectionCode().equals("1")) {
                            ib.getStop().add(e3.getStopPointName());
                            ib.setStopSize(ib.getStopSize() + 1);
                        }
                    });
                }
            });

            Collections.sort(ib.getStop());
            listOfBusLine.add(ib);
        });

        sortListDescBySopSize(listOfBusLine);
        List<BusLine> result = listOfBusLine.subList(0,
                (listOfBusLine.size()<sizeOfList?listOfBusLine.size():sizeOfList));
        sortListAscByBusLineNumber(result);
        return result;
    }

    private void sortListDescBySopSize(List<BusLine> listOfBusLine){
        Collections.sort(listOfBusLine, (o1, o2) -> o2.getStopSize() - o1.getStopSize());
    }

    private void sortListAscByBusLineNumber(List<BusLine> listOfBusLine){
        Collections.sort(listOfBusLine, (o1, o2) -> o2.getBusLine().compareTo(o1.getBusLine()));
    }

    public static TraficlabService getInstance(){
        return beanUtil.getBean(TraficlabService.class);
    }
}
