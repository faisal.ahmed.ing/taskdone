package server.core.model;

import java.util.ArrayList;
import java.util.List;

public class BusLine {
    private String busLine;
    private List<String> stop;
    private int stopSize;

    public BusLine(String busLine) {
        this.busLine = busLine;
        stop = new ArrayList<>();
        stopSize=0;
    }

    public String getBusLine() {
        return busLine;
    }

    public void setBusLine(String busLine) {
        this.busLine = busLine;
    }

    public List<String> getStop() {
        return stop;
    }

    public void setStop(List<String> stop) {
        this.stop = stop;
    }

    public int getStopSize() {
        return stopSize;
    }

    public void setStopSize(int stopSize) {
        this.stopSize = stopSize;
    }

    @Override
    public String toString() {
        return "BusLine{" +
                "busLine='" + busLine + '\'' +
                ", stop=" + stop +
                ", stopSize=" + stopSize +
                '}';
    }
}
