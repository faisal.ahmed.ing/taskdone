package server.core;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.context.ApplicationContext;


@Component
public class BeanUtil {
    @Autowired
    private ApplicationContext context;
    public <T> T getBean(Class<T> beanClass) { return context.getBean(beanClass);}
}
