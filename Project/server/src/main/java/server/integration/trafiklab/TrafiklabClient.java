package server.integration.trafiklab;

import server.integration.trafiklab.model.Jour;
import server.integration.trafiklab.model.Line;
import server.integration.trafiklab.model.Stop;

public interface TrafiklabClient {
    Line getLine();
    Stop getStop();
    Jour getJour();
}
