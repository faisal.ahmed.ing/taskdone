package server.integration.trafiklab.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(value={"ExecutionTime"})
public class Line {
    @JsonProperty("StatusCode")
    private Integer statusCode;
    @JsonProperty("Message")
    private String message;
    @JsonProperty("ResponseData")
    private ResponseDataLine responseData;

    public Line() {
    }

    public Integer getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(Integer statusCode) {
        this.statusCode = statusCode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ResponseDataLine getResponseData() {
        return responseData;
    }

    public void setResponseData(ResponseDataLine responseData) {
        this.responseData = responseData;
    }

    @Override
    public String toString() {
        return "Line{" +
                "statusCode=" + statusCode +
                ", message='" + message + '\'' +
                ", responseData=" + responseData +
                '}';
    }
}
