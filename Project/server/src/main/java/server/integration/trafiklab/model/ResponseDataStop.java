package server.integration.trafiklab.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

@JsonIgnoreProperties(value={"Version", "Type"})
public class ResponseDataStop{
    @JsonProperty("Result")
    private List<ResultStop> result;

    public ResponseDataStop() {
    }

    public List<ResultStop> getResult() {
        return result;
    }

    public void setResult(List<ResultStop> result) {
        this.result = result;
    }

    @Override
    public String toString() {
        return "ResponseDataStop{" +
                "resultStop=" + result +
                '}';
    }
}