package server.integration.trafiklab.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(value={"ExecutionTime"})
public class Stop{
    @JsonProperty("StatusCode")
    private String statusCode;
    @JsonProperty("Message")
    private String message;
    @JsonProperty("ResponseData")
    private ResponseDataStop responseData;

    public Stop() {
    }

    public ResponseDataStop getResponseData() {
        return responseData;
    }

    public void setResponseData(ResponseDataStop responseData) {
        this.responseData = responseData;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return "Stop{" +
                "statusCode='" + statusCode + '\'' +
                ", message='" + message + '\'' +
                ", responseData=" + responseData +
                '}';
    }
}



