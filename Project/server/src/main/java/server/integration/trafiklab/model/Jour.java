package server.integration.trafiklab.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(value={"ExecutionTime"})
public class Jour {
    @JsonProperty("StatusCode")
    private Integer statusCode;
    @JsonProperty("Message")
    private String message;
    @JsonProperty("ResponseData")
    private ResponseDataJour responseData;

    public Jour() {
    }

    public Integer getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(Integer statusCode) {
        this.statusCode = statusCode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ResponseDataJour getResponseData() {
        return responseData;
    }

    public void setResponseData(ResponseDataJour responseData) {
        this.responseData = responseData;
    }

    @Override
    public String toString() {
        return "Jour{" +
                "statusCode=" + statusCode +
                ", message='" + message + '\'' +
                ", responseData=" + responseData +
                '}';
    }
}
