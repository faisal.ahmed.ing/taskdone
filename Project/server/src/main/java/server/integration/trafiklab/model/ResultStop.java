package server.integration.trafiklab.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(value={
        "StopAreaNumber", "LocationNorthingCoordinate",
        "LocationEastingCoordinate", "ZoneShortName",
        "StopAreaTypeCode", "LastModifiedUtcDateTime",
        "ExistsFromDate"})
public class ResultStop{
    @JsonProperty("StopPointNumber")
    private String stopPointNumber;
    @JsonProperty("StopPointName")
    private String stopPointName;

    public ResultStop() {
    }

    public String getStopPointNumber() {
        return stopPointNumber;
    }

    public void setStopPointNumber(String stopPointNumber) {
        this.stopPointNumber = stopPointNumber;
    }

    public String getStopPointName() {
        return stopPointName;
    }

    public void setStopPointName(String stopPointName) {
        this.stopPointName = stopPointName;
    }

    @Override
    public String toString() {
        return "ResultStop{" +
                "stopPointNumber='" + stopPointNumber + '\'' +
                ", stopPointName='" + stopPointName + '\'' +
                '}';
    }
}