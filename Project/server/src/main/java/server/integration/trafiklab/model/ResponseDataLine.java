package server.integration.trafiklab.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;

@JsonIgnoreProperties(value={"Version","Type"})
public class ResponseDataLine {
    @JsonProperty("Result")
    private List<ResultLine> result;

    public ResponseDataLine() {
    }

    public List<ResultLine> getResult() {
        return result;
    }

    public void setResult(List<ResultLine> result) {
        this.result = result;
    }

    @Override
    public String toString() {
        return "ResponseDataLine{" +
                "result=" + result +
                '}';
    }
}
