package server.integration.trafiklab.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

@JsonIgnoreProperties(value={"Version", "Type"})
public class ResponseDataJour {
    @JsonProperty("Result")
    private List<ResultJour> result;

    public ResponseDataJour() {
    }

    public List<ResultJour> getResult() {
        return result;
    }

    public void setResult(List<ResultJour> result) {
        this.result = result;
    }

    @Override
    public String toString() {
        return "ResponseDataJour{" +
                "result=" + result +
                '}';
    }
}