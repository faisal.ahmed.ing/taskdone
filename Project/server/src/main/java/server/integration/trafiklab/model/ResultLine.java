package server.integration.trafiklab.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(value={
        "LineDesignation",
        "DefaultTransportModeCode",
        "LastModifiedUtcDateTime",
        "ExistsFromDate"})
public class ResultLine {
    @JsonProperty("LineNumber")
    private String lineNumber;
    @JsonProperty("DefaultTransportMode")
    private String defaultTransportMode;

    public ResultLine() {
    }

    public String getLineNumber() {
        return lineNumber;
    }

    public void setLineNumber(String lineNumber) {
        this.lineNumber = lineNumber;
    }

    public String getDefaultTransportMode() {
        return defaultTransportMode;
    }

    public void setDefaultTransportMode(String defaultTransportMode) {
        this.defaultTransportMode = defaultTransportMode;
    }

    @Override
    public String toString() {
        return "ResultLine{" +
                "lineNumber='" + lineNumber + '\'' +
                ", defaultTransportMode='" + defaultTransportMode + '\'' +
                '}';
    }
}
