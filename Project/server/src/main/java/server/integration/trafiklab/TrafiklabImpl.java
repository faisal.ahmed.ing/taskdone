package server.integration.trafiklab;


import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.InvalidPropertyException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import server.integration.trafiklab.model.Jour;
import server.integration.trafiklab.model.Line;
import server.integration.trafiklab.model.Stop;

import javax.security.auth.message.ClientAuth;
import javax.security.auth.message.config.ClientAuthConfig;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

@Service
public class TrafiklabImpl implements TrafiklabClient{

    @Value("${trafikLab.key}")
    private String trafikLabKey;

    @Value("${trafikLab.base.path}")
    private String trafikLabBasePath;

    @Value("${trafikLab.search.path.line}")
    private String trafikLabSearchPathLine;

    @Value("${trafikLab.search.path.stop}")
    private String trafikLabSearchPathStop;

    @Value("${trafikLab.search.path.jour}")
    private String trafikLabSearchPathJour;

    private final Logger log = LoggerFactory.getLogger(TrafiklabImpl.class);

    @Override
    public Line getLine(){
        try {
            String url = trafikLabBasePath+
                    "?key="+trafikLabKey+
                    "&model="+trafikLabSearchPathLine+
                    "&DefaultTransportModeCode=BUS";

            RestTemplate template = new RestTemplate();
            ResponseEntity<Line> response = template.exchange(
                    url,
                    HttpMethod.GET, getHeader(), Line.class);

            logTrafikLabMessage(response.getBody());
            return response.getBody();
        }catch(Exception ex){
            logTrafikLabKey();
            log.error("Could not fetch data for LINE",ex);
            throw ex;
        }
    }

    @Override
    public Stop getStop() {
        try {
            String url = trafikLabBasePath+
                    "?key="+trafikLabKey+
                    "&model="+trafikLabSearchPathStop;

            RestTemplate template = new RestTemplate();
            ResponseEntity<Stop> response = template.exchange(
                    url,
                    HttpMethod.GET, getHeader(), Stop.class);
            logTrafikLabMessage(response.getBody());
            return response.getBody();
        }catch(Exception ex){
            logTrafikLabKey();
            log.error("Could not fetch data for STOP",ex);
            throw ex;
        }
    }

    @Override
    public Jour getJour() {
        try {
            String url = trafikLabBasePath+
                    "?key="+trafikLabKey+
                    "&model="+trafikLabSearchPathJour+
                    "&DefaultTransportModeCode=BUS";

            RestTemplate template = new RestTemplate();
            ResponseEntity<Jour> response = template.exchange(
                    url,
                    HttpMethod.GET, getHeader(), Jour.class);
            logTrafikLabMessage(response.getBody());
            return response.getBody();
        }catch(Exception ex){
            logTrafikLabKey();
            log.error("Could not fetch data for JOUR",ex);
            throw ex;
        }
    }

    private HttpEntity<String> getHeader(){
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        return new HttpEntity<>(headers);
    }

    private void logTrafikLabKey(){
        if(trafikLabKey.equals("Need to set trafikLab key"))
            log.error(trafikLabKey);
    }

    private void logTrafikLabMessage(Object data) throws RuntimeException{
        if(data instanceof Line && ((Line)data).getResponseData()==null)
            throw new RuntimeException(((Line)data).getMessage());
        else if(data instanceof Stop && ((Stop)data).getResponseData()==null)
            throw new RuntimeException(((Stop)data).getMessage());
        else if(data instanceof Jour && ((Jour)data).getResponseData()==null)
            throw new RuntimeException(((Jour)data).getMessage());
    }

}
