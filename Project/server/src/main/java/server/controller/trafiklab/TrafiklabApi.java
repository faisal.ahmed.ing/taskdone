package server.controller.trafiklab;

import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@RequestMapping(value = "/trafiklab")
public interface TrafiklabApi {
    @RequestMapping(value="/getBusLineWithMostStop",
            method = RequestMethod.GET,
            produces =MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity getBusLineWithMostStop();
}
