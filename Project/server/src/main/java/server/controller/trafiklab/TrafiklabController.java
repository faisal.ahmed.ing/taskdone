package server.controller.trafiklab;


import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RestController;
import server.core.TraficlabService;
import server.core.model.BusLine;
import java.util.List;

@CrossOrigin("*")
@RestController
public class TrafiklabController implements TrafiklabApi {
    @Override
    public ResponseEntity getBusLineWithMostStop(){
        List<BusLine> listOfBusLine = TraficlabService.getInstance().getListOfBusLine();
        return new ResponseEntity(listOfBusLine, HttpStatus.OK);
    }
}
