package server;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import server.core.TraficlabService;
import server.core.model.BusLine;
import server.integration.trafiklab.model.Jour;
import server.integration.trafiklab.model.Line;
import server.integration.trafiklab.model.Stop;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
public class BusLineTest {

    private Line line;
    private Stop stop;
    private Jour jour;

    @BeforeEach
    public void setup(){
        line = readFileLine();
        stop = readFileStop();
        jour = readFileJour();
    }

    @Test
    public void test_fetch_4_bus_line_with_most_stop(){
        List<BusLine> listOfBusLine = TraficlabService.getInstance().createBusList(line, stop, jour, 4);
        int index=0;
        assertThat(listOfBusLine.size()).isEqualTo(4);
        assertThat(listOfBusLine.get(index).getBusLine() ).isEqualTo("117");
        assertThat(listOfBusLine.get(index).getStop().size() ).isEqualTo(21);
        assertThat(listOfBusLine.get(index).getStop().size() ).isEqualTo(listOfBusLine.get(index).getStopSize());

        index=3;
        assertThat(listOfBusLine.get(index).getBusLine() ).isEqualTo("1");
        assertThat(listOfBusLine.get(index).getStop().size() ).isEqualTo(31);
        assertThat(listOfBusLine.get(index).getStop().size() ).isEqualTo(listOfBusLine.get(index).getStopSize());
    }

    private Line readFileLine(){
        Line line = null;
        try{
            ObjectMapper objectMapper = new ObjectMapper();
            line = objectMapper.
                    readValue(new File("src/test/resources/line.json"), Line.class);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return line;
    }

    private Stop readFileStop(){
        Stop stop = null;
        try{
            ObjectMapper objectMapper = new ObjectMapper();
            stop = objectMapper.
                    readValue(new File("src/test/resources/stop.json"), Stop.class);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return stop;
    }

    private Jour readFileJour(){
        Jour jour = null;
        try{
            ObjectMapper objectMapper = new ObjectMapper();
            jour = objectMapper.
                    readValue(new File("src/test/resources/jour.json"), Jour.class);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return jour;
    }
}
