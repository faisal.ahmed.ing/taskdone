# TaskDone

Maven 3.8.1 and OpenJdk 16 is used for development

Source Code is in Project

### Run the application
- The server jar file is in RunApplication

java -DtrafikLab_key="<Set Your TrafikLab Key>" -jar SBABTask-0.0.1-SNAPSHOT.jar
- The client file is in RunApplication/client

open the client index.html in browser


### Note
The cache in springframework is not used but can be implemented for request to trafiklab because they are updating their data once a day so its not necessary to maka several requests to same api in one day. Instead we can read the data from chache and renew it once a day. 

Logging needs more love :-)
